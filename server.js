var socket = require('./src/server/io')
var http = require('./src/server/http')

var socket_port = 1234
var http_port = 3000

console.log('[ SOCKET ] Socket server started on port '+ socket_port )
socket.listen(socket_port)

if(process.env.NODE_ENV === "production"){
	console.log('[ HTTP ] HTTP server started on port '+ http_port )
	http.listen(http_port)
}
