var prod = process.env.NODE_ENV === "production";
var webpack = require('webpack');
var path = require('path');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

var css = {
  load: "style-loader!css-loader!stylus-loader",
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
  ]
}

if(prod){
  css.load = ExtractTextPlugin.extract({ fallbackLoader: "style-loader", loader:"css-loader!stylus-loader"})
  css.plugins = [
    new ExtractTextPlugin("bundle.css")
  ]
}

module.exports = {
  context: path.join(__dirname, "src"),
  entry: [
    "./js/App.js",
    'webpack-hot-middleware/client'
  ],
  output: {
    path: __dirname+'/public/',
    filename: "bundle.js",
    chunkFilename: "[id].js"
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: ['react', 'es2015']
        }
      },
      { 
        test: /\.styl$/, 
        loader: css.load, 
        exclude: /node_modules/ 
      }
    ]
  },
  plugins: css.plugins
};
