var storage = require('./storage')
var io = require('socket.io')()

var model = storage.model
var initId = storage.initId

var comments = []
var id = 1;

initId((newId) => id = newId)

// Communication 
io.on('connection', function(socket){
  	socket.on('new-comment', function(comment){
  		response('new-comment', comment, io)
  	});
  	socket.on('remove-comment', function(comment){
	  	response('remove-comment', comment, io)
  	});
  	socket.on('fetch-comment', function(number){
	  	response('fetch-comment', number, socket)
  	});
});

// Response generator
var response = (label, data, remote) => {
	var res = {}
	var done = (input) => {
		res.total = comments.length
		remote.emit(input || label, res)
	}

	switch(label){
		case 'new-comment': {
			// Saving comment on DB
			data.id = id++
			var comment = new model(data)
			comment.save((err) => {
				if(err)
					console.log(err)
				comments.push(comment)
				res.comment = comment
				done()
			})
			break
		}
		case 'remove-comment': {
			// Removing comment on DB
			model.find({ id: data }).remove( (err) => {
				if(err) throw err
				comments = comments.filter((item) => { return item.id !== data })
				res.id = data
				done()
			})
			break
		}
		case 'fetch-comment': {
			// Getting comments from DB
			model.find({}, (err, db_comments) => { 
				comments = db_comments 
				model.find({})
					.sort({'date': -1})
					.limit(data || Infinity)
					.exec(function(err, limited_comments) {
						res.comments = limited_comments.reverse()
						done()
					});
			})
			break
		}
	}
}

module.exports = io