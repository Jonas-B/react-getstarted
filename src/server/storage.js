var mongoose = require('mongoose');

// DB
mongoose.connect('mongodb://localhost/comments');

// Comment Model
var model = mongoose.model('comment', mongoose.Schema({
	id : Number,
  	author : String,
  	content : String,
  	date : { type : Date, default : Date.now }
},
{ 
	id: false,
	versionKey: false 
}))

var initId = (cb) => {
	model.findOne({}, {}, { sort: { 'id': -1 } }, function(err, comment){
		if(err) cb(err)
		cb(comment.id+1)
	})
}

module.exports = { model, initId }