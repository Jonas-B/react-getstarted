import React from "react";
import ReactDOM from "react-dom";

import CommentBox from './components/CommentBox'

import '../css/index.styl'
// Importing some content from ./components/...

let App = React.createClass({
	ready() {
		let loader = document.querySelector('#load')
		
		setTimeout(() => {
			loader.classList.add('done')
			setTimeout(() => loader.remove(), 500)
		}, 100)
	},
	render () {
		return (
			<CommentBox ready={this.ready}/>
		)
	}
})

const app = document.getElementById('app');

ReactDOM.render(
	<App />, 
	app
);
