import io from 'socket.io-client'

let data = {
	socket: io.connect('http://192.168.1.32:1234'),
	fetchOn: (content, items, max, cb = () => {}) => {
		console.log("[Received new comment]", content)
		var arr = []
		if(items.length === max) arr = items.slice(1)
		else arr = items.slice()
		cb(arr.concat([content.comment]), content.total)
	},
	fetchOff: (content, items, max, cb = () => {}) => {
		console.log("[Received removed comment]", content)
		var tmp = [].concat(items)
		var comments = tmp.filter((item) => { return content.id !== item.id })
		cb(comments, content.total)
	},
	fetchAll: (content, cb = () => {}) => {
		var items = content.comments || []
		console.log("[Received all comment]", items)
		cb(items, content.total)
	},
	add: function(item, cb = () => {}) {
		console.log("[Sending new comment]", item)
		data.socket.emit('new-comment', item)
		cb(item)
	},
	remove: function(id, cb = () => {}) {
		console.log("[Sending removed comment]", id)
		data.socket.emit('remove-comment', id)
		cb(id)
	},
	update: function(items) {
		if(items) data.socket.emit('fetch-comment', items);
		else data.socket.emit('fetch-comment');
	}
}

export default data