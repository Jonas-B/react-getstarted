let dateStr = (input) => {
	let date = input || new Date()

	let day = add0(date.getDate())+'-'+add0(date.getMonth()+1)+'-'+add0(date.getFullYear())

	let hour = add0(date.getHours())+':'+add0(date.getMinutes())+':'+add0(date.getSeconds())

	return day+', '+hour
}

let add0 = (input) => {
	return (input<10)?'0'+input:input
}

export default dateStr