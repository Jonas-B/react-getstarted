import React from "react";
import dateStr from '../util/date'
import io from 'socket.io-client'

import CommentList from './CommentList'
import CommentForm from './CommentForm'

import dataFactory from '../data/socket'

export default  React.createClass({
	dataManager: dataFactory,
	getDefaultProps () {
		return { loadNumber: 10 }
	},
	getInitialState () {
		return { comments: [], totalItems: 0, totalPrinted: this.props.loadNumber}
	},
	componentDidMount () {
		this.dataManager.socket.on('new-comment', (response) => {
			this.dataManager.fetchOn(response, this.state.comments, this.state.totalPrinted, (comments, totalItems) => {
				this.setState({ comments, totalItems })
			})
		})

		this.dataManager.socket.on('remove-comment', (response) => {
			this.dataManager.fetchOff(response, this.state.comments, this.state.totalPrinted, (comments, totalItems) => {
				this.setState({ comments, totalItems })
			})
		});

		this.dataManager.socket.on('fetch-comment', (response) => {
			this.dataManager.fetchAll(response, (comments, totalItems) => {
				this.setState({ comments, totalItems })
				this.props.ready()
			})
		})

		this.dataManager.update(this.state.totalPrinted)
	},
	loadMore () {
		this.setState({ totalPrinted: this.state.totalPrinted + this.props.loadNumber }, () => this.dataManager.update(this.state.totalPrinted))
	},
	render () {
		return (
			<div className='commentBox'>
				<CommentForm 	onSubmit={this.dataManager.add} maxPseudo={ 20 }/>
				<CommentList 	comments={this.state.comments} 
								total={this.state.totalItems} 
								loadMore={this.loadMore} 
								remove={this.dataManager.remove} 
								loadNumber={this.props.loadNumber} />
			</div>
		)
	}
})