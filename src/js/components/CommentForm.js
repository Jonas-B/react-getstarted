import React from "react";
import MInput from "./presentation/ManagedInput"

var css = {
	max: 'max',
	anim: 'animated',
	style: 'shake',
	make: function(){ return this.max+' '+this.anim+' '+this.style }
}

export default  React.createClass({
	getDefaultProps () {
		return { maxChar: 140, maxPseudo: 15 }
	},
	handleSubmit (e) {
		if(e) e.preventDefault()
		let { author, content } = this.refs
		let authorInput = author.refs.input
		let contentInput = content.refs.input
		if(!this._check([authorInput, contentInput])){
			this.props.onSubmit({ author: authorInput.value, content: contentInput.value })
			content.setState(content.reset())
		}
	},
	_check (values) {
		var tmp = values.filter((e) => !e.value || 0 === e.value.length)
		tmp.forEach((e) => {
			this._animReset(e)
			setTimeout(() => {
				this._anim(e)
			}, 1)
		})
		return tmp.length
	},
	_animReset (e){
		e.classList.remove(css.anim)
		e.classList.remove(css.style)
	},
	_anim (e) {
		e.classList.add(css.anim)
		e.classList.add(css.style)
	},
	render () {
		return (
			<form className="commentForm" onSubmit={this.handleSubmit}>
				<h2 className='commentFormTitle'>Your</h2>
				<div className='commentFormContent'>
					<MInput ref="author" label='Your name' placeholder="sweety_name" maxChar={ this.props.maxPseudo } css={ css }/>
					<MInput type='textarea' ref="content" label='Your comment' placeholder="This is awesome !" maxChar={ this.props.maxChar } css={ css } />
			        <input type="submit" value="Post"/>
			    </div>
		    </form>
		)
	}
})