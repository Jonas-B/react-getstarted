import React from "react";
import { markdown } from "markdown"
import dateStr from "../util/date"

export default  React.createClass({
	getInitialState () {
		return { hover: '', css: {} }
	},
	mouseEnter () {
		this.setState({ hover: ' active' })
	},
	mouseLeave () {
		this.setState({ hover: '' })
	},
	handleRemove () {
		let value = this._outerHeight()
		this.setState({ css: { 
			opacity: 0,
			marginTop: -value,
			transition: "all 500ms ease",
			zIndex: 0
		} })

		setTimeout(this.props.remove, 500)
	},
	_content () { 
		return this.props.children
	},
	_outerHeight () {
		let e = this.refs.component
		let height = e.clientHeight
		let margin = parseInt(window.getComputedStyle(e, null).getPropertyValue('margin-bottom').split('px')[0])
		return height + margin
	},
	_author () {
		let content = this.props.author.substring(1,this.props.author.length)
		let first = this.props.author.substring(0, 1)
		return <h3 className='commentAuthor'><span className='firstLetter'>{ first }</span>{ content }</h3>
	},
	_date () {
		return dateStr(new Date(this.props.time))
	},
	componentDidMount () {
		if(this.props.animate){
			let value = this._outerHeight()
			this.setState({ css: {
				opacity: 0,
				marginTop: -value
			} })
			setTimeout(() => {
				this.setState({ css: { 
					opacity: 1,
					marginTop: 0,
					transition: "all 500ms ease"
				} })
			}, 1)
		}
	},
	render () {
		return (
			<div ref="component" className='comment' onMouseEnter={this.mouseEnter} onMouseLeave={this.mouseLeave} style={this.state.css}>
				<span className={ 'commentClose' + this.state.hover } onClick={this.handleRemove}>X</span>
				{ this._author() }
				<div className='commentContent'>{ this._content() }</div>
				<span className='commentTime'>{ this._date() }</span>
				<span className='commentIndex'>{ '#'+this.props.index }</span>
			</div>
		)
	}
})