import React from "react";

export default  React.createClass({
	getDefaultProps () {
		return { maxChar: 20 , type: 'text', css: { max: 'max', make: function() { return this.max+' animated' } }}
	},
	getInitialState () {
		return { value: '', currentChar: 0, class: '' }
	},
	reset () {
		return { value: '', currentChar: 0, class: '' }
	},
	handleChange (e) {
		if(e.target.value.length <= this.props.maxChar)
			this.setState({ value: e.target.value, currentChar: e.target.value.length, class: '' })
		else{
			this.setState({ class: this.props.css.max })
			setTimeout(() => this.setState({ class: this.props.css.make() }) , 1);
		}
	},
	_input () {
		if(this.props.type === 'textarea') return <textarea ref="input" placeholder={ this.props.placeholder } value={this.state.value} onChange={this.handleChange} />
		return <input type={ this.props.type } ref="input" placeholder={ this.props.placeholder } value={this.state.value} onChange={this.handleChange} />
	},
	render () {
		return (
			<div className="managedInput">
				<label> { this.props.label }
		        	{ this._input() }
		        </label>
		        <div ref="currentChar" className={ 'current-char '+this.state.class } >{ this.state.currentChar + '/' + this.props.maxChar }</div>
		    </div>
		)
	}
})