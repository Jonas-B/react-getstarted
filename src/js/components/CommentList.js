import React from "react";
import Comment from './Comment'

export default  React.createClass({
	anim: false,
	
	componentDidUpdate() {
		this.anim = true
	},
	_content () {
		return this.props.comments.map(function(item, i){
			return <Comment animate={ this.anim } author={ item.author } time={ item.date } key={ item.id } index={ item.id } remove={() => this.props.remove(item.id)}>{ item.content }</Comment>
		}.bind(this))
	},
	_infos () {
		if(this.props.comments.length) return <span className='textInfo'> ({ this.props.comments.length }/{ this.props.total })</span>
	},
	_nextLoad () {
		let next = this.props.comments.length+this.props.loadNumber;
		if(next < this.props.total) return next
		else return this.props.total
	},
	_footer () {
		if(!this.props.comments.length)
			return <span>There is no comments yet.</span>
		else if(this.props.comments.length === this.props.total)
			return <span>All comments are displayed.</span>
		else
			return <button className='commentLoadMore' onClick={ this.props.loadMore }>Load more ({ this._nextLoad() }/{ this.props.total })</button>
	},
	render () {
		return (
			<div className='commentList'>
				<h2 className='commentListTitle'>Comments { this._infos() }</h2>
				<div className='commentListContent'>{ this._content() }</div>
				<div className='commentListFooter'>{ this._footer() }</div>
			</div>
		)
	}
})